<?php get_header(); ?>

<section class="container" id="single-post">
	<div class="row">
		<div class="col-md-9" id="page">
			<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
			<h1><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
			<div class="page-body">
				<?php the_content('(continue reading...)'); ?>
			</div>
			<?php endwhile; ?>
			<h1><a href="#">Not Found</a></h1>
			<div class="page-body">
				<p>Sorry, but you are looking for something that isn't here. You can search again by using this form.</p><div class="col-sm-3">><?php get_search_form( $echo ); ?></div>
			</div>
			<?php else : ?>
		</div>
		<?php get_sidebar(); ?>
	</div>
</section>

<?php get_footer(); ?>