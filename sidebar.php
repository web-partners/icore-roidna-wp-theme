<div class="col-md-3" id="blog-sidebar">
    <?php include (TEMPLATEPATH . '/welcome.php'); ?>
    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar_full') ) : ?>
    <div id="recent-posts">
        <h3>Recent Posts</h3>
        <ul>
            <?php wp_get_archives('type=postbypost&limit=10'); ?>
        </ul>
    </div>
    <?php endif; ?>
    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar_left') ) : ?>
    <div id="categories">
        <h3>Categories</h3>
        <ul>
            <?php wp_list_categories('show_count=0&title_li='); ?>
        </ul>
    </div>
    <div id="by-tags">
        <h3>Browse by tags</h3>
        <?php wp_tag_cloud('smallest=8&largest=17&number=30'); ?>
    </div>
    <?php endif; ?>
    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer_right') ) : ?>
    <div id="archives">
        <h3>Archives</h3>
        <ul>
            <?php wp_get_archives('type=monthly&limit=12'); ?>
        </ul>
    </div>
    <?php endif; ?>
    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer_left') ) : ?>
    <div id="search">
        <h3>Looking for something?</h3>
        <p>Use the form below to search the site:</p>
        <?php include (TEMPLATEPATH . '/searchform.php'); ?>
        <p>Still not finding what you're looking for? Drop a comment on a post or contact us so we can take care of it!</p>
    </div>
    <?php endif; ?>
</div>