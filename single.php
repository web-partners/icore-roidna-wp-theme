<?php get_header(); ?>

<section class="container" id="single-post">
	<div class="row">
		<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
		<div class="col-md-9" id="page">
			<h1><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
			<div class="page-body">
				<?php the_content('(continue reading...)'); ?>
			</div>
			<div id="comments clearfix">
				<?php if (function_exists('wp_list_comments')): ?>
				<!-- WP 2.7 and above -->
				<?php comments_template('', true); ?>

				<?php else : ?>
				<!-- WP 2.6 and below -->
				<?php comments_template(); ?>
				<?php endif; ?>
			</div>
			<div id="rss">
				<span id="rssleft"><?php comments_rss_link(__('<abbr title="Really Simple Syndication">RSS</abbr> feed for this post (comments)')); ?></span>
				<span id="trackright"><?php if ( pings_open() ) : ?> &#183; <a href="<?php trackback_url() ?>" rel="trackback"><?php _e('TrackBack <abbr title="Uniform Resource Identifier">URI</abbr>'); ?></a><?php endif; ?></span>
			</div>
		</div>
		<?php endwhile; ?>
		<?php else : ?>
		<div class="col-md-9" id="page">
			<h1><a href="#">Not Found</a></h1>
			<div class="page-body">
				<p>Sorry, but you are looking for something that isn't here. You can search again by using this form.</p><div class="col-sm-3">><?php get_search_form( $echo ); ?></div>
			</div>
		</div>
		<?php endif; ?>
		<?php get_sidebar(); ?>
	</div>
</section>

<?php get_footer(); ?>