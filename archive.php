<?php get_header(); ?>

<section class="container" id="blog-feed">
	<div class="row">
		<div class="col-md-9" id="posts">
			<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
			<?php /* If this is a category archive */ if (is_category()) { ?>
			<h3 class="pageTitle"><?php single_cat_title(); ?></h3>
			<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
			<h3 class="pageTitle">Tag: <?php single_tag_title(); ?></h3>
			<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
			<h3 class="pageTitle">Archive for <?php the_time('F jS, Y'); ?></h3>
			<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
			<h3 class="pageTitle">Archive for <?php the_time('F, Y'); ?></h3>
			<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
			<h3 class="pageTitle">Archive for <?php the_time('Y'); ?></h3>
			<?php /* If this is an author archive */ } elseif (is_author()) { ?>
			<h3 class="pageTitle">Author Archive</h3>
			<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
			<h3 class="pageTitle">Blog Archives</h3>
			<?php } ?>
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
					<div class="post">
						<div class="post-header">
							<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
							<span>by <?php the_author_posts_link(); ?> on <?php the_time('M.d, Y') ?>, under <?php the_category(', '); ?></span>
						</div>
						<div class="post-body">
							<?php the_content('(continue reading...)'); ?>
						</div>
						<div class="post-footer">
							<?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?> <?php the_tags('<span>Tags: </span>', ', ', ''); ?> <a href="<?php the_permalink() ?>">Read More</a>
						</div>
					</div>
				<?php endwhile; ?>
			<?php else : ?>
				<div class="post">
					<div class="post-header">
						<h3><a href="#">Not Found</a></h3>
					</div>
					<div class="post-body">
						<p>Sorry, but you are looking for something that isn't here. You can search again by using this form.</p><p><?php get_search_form( $echo ); ?></p>
					</div>
				</div>
			<?php endif; ?>
			<div class="clearfix" id="next-prev">
                <div class="pull-left">
                    <?php posts_nav_link('','','&laquo; Previous Entries') ?>
                </div>
                <div class="pull-right">
                    <?php posts_nav_link('','Next Entries &raquo;','') ?>
                </div>
            </div>
		</div>
		<?php get_sidebar(); ?>
	</div>
</section>

<?php get_footer(); ?>