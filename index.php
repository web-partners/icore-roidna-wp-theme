<?php get_header(); ?>

<section class="container" id="blog-feed">
	<div class="row">
		<div class="col-md-9" id="posts">
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
					<div class="post">
						<div class="post-header">
							<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
							<span>by <?php the_author_posts_link(); ?> on <?php the_time('M.d, Y') ?>, under <?php the_category(', '); ?></span>
						</div>
						<div class="post-body">
							<?php the_content('(continue reading...)'); ?>
						</div>
						<div class="post-footer">
							<?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?> <?php the_tags('<span>Tags: </span>', ', ', ''); ?> <a href="<?php the_permalink() ?>">Read More</a>
						</div>
					</div>
				<?php endwhile; ?>
			<?php else : ?>
				<div class="post">
					<div class="post-header">
						<h3><a href="#">Not Found</a></h3>
					</div>
					<div class="post-body">
						<p>Sorry, but you are looking for something that isn't here. You can search again by using this form.</p><p><?php get_search_form( $echo ); ?></p>
					</div>
				</div>
			<?php endif; ?>
			<div class="clearfix" id="next-prev">
                <div class="pull-left">
                    <?php previous_posts_link() ?>
                </div>
                <div class="pull-right">
                    <?php next_posts_link() ?>
                </div>
            </div>
		</div>
		<?php get_sidebar(); ?>
	</div>
</section>

<?php get_footer(); ?>