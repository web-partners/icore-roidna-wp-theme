<!DOCTYPE html>
<html lang="en">
    <!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
    <!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8 ie7" lang="en"> <![endif]-->
    <!--[if IE 8]>    <html class="no-js lt-ie9 ie8" lang="en"> <![endif]-->
    <!--[if gt IE 8]><!--><!--<![endif]-->
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
        <meta name="robots" content="follow, all" />
        <title><?php if (is_home () ) { bloginfo('name'); echo " - "; bloginfo('description'); 
        } elseif (is_category() ) {single_cat_title(); echo " - "; bloginfo('name');
        } elseif (is_single() || is_page() ) {single_post_title(); echo " - "; bloginfo('name');
        } elseif (is_search() ) {bloginfo('name'); echo " search results: "; echo wp_specialchars($s);
        } else { wp_title('',true); }?></title>
        
        <link rel="stylesheet" id="main-styles-css" href="<?php bloginfo('template_url'); ?>/style.css" type="text/css" media="all">
        <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico">
        <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        <?php wp_head(); ?>
    </head>
    <body>
        <!--[if lt IE 10]>
          <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- # Info bar -->
        <div class="infobar-wrapper">
            <div class="container infobar">
                <div class="row">
                    <div class="info-main pull-right">
                        <div class="phone">
                            <span class="icon-phone"></span>
                            <span class="phone-label">1-866-949-4267</span>
                        </div> 
                        <a href="http://www2.icore.com/customer-support.aspx#login" class="login">
                            <span class="icon-user"></span>
                            <span class="login-label">Login</span>
                        </a>                                                                           
                    </div>  
                </div>
            </div>
        </div>
        
        <!-- # Navigation bar (primary nav) -->
        <div class="navibar-wrapper">
            <header class="container navibar">
                <div class="row">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <a href="javascript:;" class="nav-tert-link pull-right">
                                <span class="sr-only">Navigation</span>
                                <span class="icon-hamburger"></span>
                                </a>
                                <span class="divider pull-right"></span>
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span>Menu</span>
                                </button>
                                <h1 class="navbar-brand text-hide img-logo">
                                    <a href="http://blog.icore.com">wp_title();</a>
                                </h1>
                            </div>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="dropdown dropdown-alt">
                                        <a href="http://www.icore.com/solutions/" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        Solutions                            </a>
                                        <ul class="dropdown-menu sm row" role="menu">
                                            <li class="col-xs-12 col-sm-4 menu-overview">
                                                <div style="" class="thumbnail dropdown-menu-content">
                                                    <h2 style=""><a href="http://www.icore.com/solutions/">Overview</a></h2>
                                                </div>
                                            </li>
                                            <li class="col-xs-12 col-sm-4 ">
                                                <div href="#" class="thumbnail dropdown-menu-content">
                                                    <h2>
                                                        <a href="http://www.icore.com/solutions/ucaas/">
                                                        Unified Communications                                            </a>
                                                    </h2>
                                                    <ul>
                                                        <li class="">
                                                            <a href="http://www.icore.com/solutions/ucaas/internet-phone-business-voip-systems/">
                                                            Internet Phone                                            </a>
                                                        </li>
                                                        <li class="">
                                                            <a href="http://www.icore.com/solutions/ucaas/im-presence/">
                                                            IM &amp; Presence                                            </a>
                                                        </li>
                                                        <li class="">
                                                            <a href="http://www.icore.com/solutions/ucaas/web-collaboration-conferencing/">
                                                            Web Collaboration                                            </a>
                                                        </li>
                                                        <li class="">
                                                            <a href="http://www.icore.com/solutions/ucaas/click-call/">
                                                            Click To Call                                            </a>
                                                        </li>
                                                        <li class="">
                                                            <a href="http://www.icore.com/solutions/ucaas/hosted-exchange-cloud-email/">
                                                            Email                                            </a>
                                                        </li>
                                                        <li class="">
                                                            <a href="http://www.icore.com/solutions/ucaas/contact-center/">
                                                            Contact Center                                            </a>
                                                        </li>
                                                        <li class="">
                                                            <a href="http://www.icore.com/solutions/ucaas/security-networking-firewall-vpn/">
                                                            Network Services                                            </a>
                                                        </li>
                                                        <li class="">
                                                            <a href="http://www.icore.com/solutions/ucaas/microsoft-lync-business-messaging/">
                                                            Lync as a Service                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="col-xs-12 col-sm-4 ">
                                                <div href="#" class="thumbnail dropdown-menu-content">
                                                    <h2>
                                                        <a href="http://www.icore.com/solutions/cloud/">
                                                        Cloud Services                                            </a>
                                                    </h2>
                                                    <ul>
                                                        <li class="">
                                                            <a href="http://www.icore.com/solutions/cloud/infrastructure-as-a-service-iaas/">
                                                            Infrastructure as a Service                                            </a>
                                                        </li>
                                                        <li class="">
                                                            <a href="http://www.icore.com/solutions/cloud/virtual-desktop-infrastructure-vdi/">
                                                            Virtual Desktop                                            </a>
                                                        </li>
                                                        <li class="">
                                                            <a href="http://www.icore.com/solutions/cloud/disaster-recovery-business-continuity/">
                                                            Disaster Recovery and Backup                                            </a>
                                                        </li>
                                                        <li class="">
                                                            <a href="http://www.icore.com/solutions/cloud/web-content-filtering/">
                                                            Web Content Filtering                                            </a>
                                                        </li>
                                                        <li class="">
                                                            <a href="http://www.icore.com/solutions/cloud/document-management/">
                                                            Document Management                                            </a>
                                                        </li>
                                                        <li class="">
                                                            <a href="http://www.icore.com/solutions/cloud/exchange-virtual-fax/">
                                                            Email                                            </a>
                                                        </li>
                                                        <li class="">
                                                            <a href="http://www.icore.com/solutions/cloud/outsourced-it/">
                                                            Outsourced IT                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="col-xs-12 col-sm-4 ">
                                                <div href="#" class="thumbnail dropdown-menu-content">
                                                    <h2>
                                                        <a href="http://www.icore.com/solutions/all-in-one/">
                                                        all-in-one                                            </a>
                                                    </h2>
                                                    <p>iCore is leading the revolution as the first provider to bring customized Unified Communications and Cloud Services together for an all-in-one user experience.</p>
                                                    <p style="padding-bottom:8px !important;padding-top:0 !important">So what’s all-in-one?</p>
                                                    <p><a href="/solutions/all-in-one/" style="text-align:left;float:left;">Learn More</a></p>
                                                    <p></p>
                                                    <ul>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class=" ">
                                        <a href="http://www.icore.com/consulting/" class="">
                                        Consulting</a>
                                    </li>
                                    <li class=" ">
                                        <a href="http://www2.icore.com/resources.aspx" class="">
                                        Resources</a>
                                    </li>
                                    <li class="dropdown ">
                                        <a href="http://www.icore.com/about/" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        About                            </a>
                                        <ul class="dropdown-menu sm row" role="menu">
                                            <li class="col-xs-12 col-sm-4 menu-overview">
                                                <div style="" class="thumbnail dropdown-menu-content">
                                                    <h2 style=""><a href="http://www.icore.com/about/">Overview</a></h2>
                                                </div>
                                            </li>
                                            <li class="col-xs-12 col-sm-4 ">
                                                <div href="#" class="thumbnail dropdown-menu-content">
                                                    <h2>
                                                        <a href="http://www.icore.com/about/partners/">
                                                        Partners                                            </a>
                                                    </h2>
                                                    <p>Join companies like Microsoft, Nutanix, and Oracle. Become an iCore Partner.</p>
                                                    <ul>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="col-xs-12 col-sm-4 ">
                                                <div href="#" class="thumbnail dropdown-menu-content">
                                                    <h2>
                                                        <a href="http://www.icore.com/about/contact/">
                                                        Contact                                            </a>
                                                    </h2>
                                                    <p>Have a question? Contact us here.</p>
                                                    <ul>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class=" ">
                                        <a href="http://www.icore.com/support/" class="">
                                        Support</a>
                                    </li>
                                    <li class="nav-tert-link-wrapper">
                                        <a href="javascript:;" class="nav-tert-link">
                                        <span class="sr-only">Navigation</span>
                                        <span class="icon-hamburger"></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.navbar-collapse -->
                        </div>
                        <!-- /.container-fluid -->
                    </nav>
                </div>
            </header>
        </div>