jQuery(document).ready(function () {
    var sidebar = jQuery(".sidebar-overlay"),
        hamburger = jQuery(".nav-tert-link"),
        mqMinSmall = "all and (min-width: 768px)",
        mqMinMedium = "all and (min-width: 992px)";
    
    // Match heights
    var i, elements = [
        ////  Solutions module
        '.m-solu .sub-m h2',
        '.m-solu .sub-m img',
        '.m-solu .sub-m p',
        ////  Support assessment module
        '.m-supasse .sub-m h2',
        '.m-supasse .sub-m img',
        '.m-supasse .sub-m p',
        //// 'Products included with this solution' module 
        '.m-solprod .sub-m img',
        '.m-solprod .sub-m h2',
        '.m-solprod .sub-m p',
        ////  Solutions content module
        '.m-solcont h2',
        '.m-solcont img',
        '.m-solcont ul',
        ////  Partners included module
        '.m-icoreso .sub-m img',
        '.m-icoreso .sub-m h2',
        '.m-icoreso .sub-m p',
        ////  Industries module
        '.m-indu .sub-m img',
        '.m-indu .sub-m h2',
        ////  Numbers module
        '.m-numb .sub-m figure img', 
        '.m-numb .sub-m figcaption',
        ////  About iCore module
        '.m-icoabou .sub-m figure img',
        '.m-icoabou .sub-m figcaption',
        ////  Resources & Tools module
        '.m-reso .sub-m img',
        '.m-reso .sub-m h2', 
        ////  Testmonials module
        //'.m-testimonials .testimonial',
        ////  Partners module
        '.m-partners img',
        ////  Clients module
        '.m-clients img',
        ////  Products included module
        '.m-proincl .sub-m',     
        ////  Team members module 
        '.m-team a img',
        '.m-team a h2',
        '.m-team a h3',
        ////  Ticket submit module
        '.m-ticsubm .sub-m h2',
        '.m-ticsubm .sub-m p',
        ////  Support resources
        '.m-suppreso h3',
        ////  Footer
        '.footer-top > div:not(.footer-logo-wrapper)'
    ];

    for (i = 0; i < elements.length; i++) {
        jQuery(elements[i]).responsiveEqualHeightGrid();
    }
    
    jQuery('.m-testimonials .testimonial').equalHeight();

    jQuery(document).on('click mouseover', '.navibar .dropdown-toggle', function() {
        jQuery('.navibar .dropdown .dropdown-menu > li > .dropdown-menu-content > h2').responsiveEqualHeightGrid(); 

        jQuery('.navibar .dropdown .dropdown-menu > li > .dropdown-menu-content').responsiveEqualHeightGrid();
    });
    

    
    
    // Show/hide sidebar
    // - with hamburger icon 
    hamburger.on("click", function(){    
        if(sidebar.css("display") == "none") {
            sidebar.fadeIn("fast");
            hamburger.addClass("active");
            
        }
        else {
            sidebar.hide();
            hamburger.removeClass("active");
        }
    });  
    
    jQuery(".sidebar-overlay").on("click", function(){
        jQuery(this).hide();
        hamburger.removeClass("active");
    });
    
    // - close with Close link
    jQuery(".sidebar-close").on("click", function(){
        jQuery(".sidebar-overlay").hide();
        hamburger.removeClass("active");
    });
    
    // - close with swipe right
    //    jQuery("body").on("swiperight", function(){
    //        jQuery(".sidebar-overlay").hide();
    //    });

    //    sidebar.on("swiperight", function(){
    //        jQuery(this).hide();
    //    });

        // - close with the click outside of the sidebar
    //    jQuery(document).on("click", function(e){
    //        if(sidebar.css("display") == "block") {
    //            sidebar.hide();
    //        }
    //    });

    //    if(jQuery(".nav-tert-link").css("display") == "none") {
    //        jQuery(document).on("click", function(){
    //            jQuery(".sidebar").fadeOut();
    //        });
    //    }
    //
    //    jQuery(".sidebar").on("click", function(){
    //        return false;
    //    });

    // Click on primary nav should take you to the href
    if (matchMedia(mqMinSmall).matches) {
        jQuery('.navibar .nav > .dropdown > a').click(function(){
            location.href = this.href;
        });
    } 
    
    var mql = matchMedia(mqMinSmall);
    mql.addListener(function(mql) {
        if (mql.matches) {
            jQuery('.navibar .nav > .dropdown > a').click(function(){
                location.href = this.href;
            });
        }
        else {
            jQuery('.navibar .nav > .dropdown > a').unbind('click');
        }
    });
     
    
    jQuery(".current.active").parents('li').first().addClass("active parent").parents('li').first().addClass("active parent");
    

});
