<div class="footer-wrapper">
            <footer class="container">
                <div class="row">
                    <div class="footer-top clearfix">
                        <div class="col-xxs-6 col-xs-5th-1 col-md-2" style="height: 154px;">
                            <a href="http://www.icore.com/about/" class="footer-heading">
                            About                                </a>
                            <a href="http://www.icore.com/about/contact/" class="">
                                About iCore                                                
                            </a>
                            <a href="http://www2.icore.com/resources/training.aspx" class="">
                                Careers                                                
                            </a>
                            <a href="http://www2.icore.com/resources/satisfied-clients.aspx" class="">
                                Videos                                                
                            </a>
                        </div>
                        <div class="col-xxs-6 col-xs-5th-1 col-md-2" style="height: 154px;">
                            <a href="http://www.icore.com/solutions/" class="footer-heading">
                            Solutions                                </a>
                            <a href="http://www.icore.com/solutions/ucaas/" class="">
                                Unified Communications                                                
                            </a>
                            <a href="http://www.icore.com/solutions/cloud/" class="">
                                Cloud Services                                                
                            </a>
                            <a href="http://www.icore.com/solutions/all-in-one/" class="">
                                all-in-one                                                
                            </a>
                        </div>
                        <div class="col-xxs-6 col-xs-5th-1 col-md-2" style="height: 154px;">
                            <a href="http://www2.icore.com/resources.aspx" class="footer-heading">
                            Resources                                </a>
                            <a href="http://www.icore.com/support/" class="">
                                Support                                                
                            </a>
                            <a href="http://www2.icore.com/news-events/press-releases.aspx" class="">
                                Press                                                
                            </a>
                        </div>
                        <div class="col-xxs-6 col-xs-5th-1 col-md-2" style="height: 154px;">
                            <a href="http://www2.icore.com/customer-support/documentation.aspx" class="footer-heading">
                            Help                                </a>
                            <a href="http://www2.icore.com/customer-support/faqs.aspx" class="">
                                FAQs                                                
                            </a>
                        </div>
                        <div class="col-xxs-6 col-xs-5th-1 col-md-2 footer-connect" style="height: 154px;">
                            <a href="http://www.icore.com/about/contact/" class="footer-heading">
                            Connect with us                                </a>
                            <a href="http://blog.icore.com/" class="">
                                Blog                                                
                            </a>
                            <div class="social-wrapper">
                                <a href="https://www.facebook.com/iCoreNetworks" class="social facebook                                                        text-hide">
                                    Facebook                                                
                                </a>
                                <a href="https://twitter.com/icore_networks" class="social twitter                                                        text-hide">
                                    Twitter                                                
                                </a>
                                <a href="https://plus.google.com/+icorenetworks/posts" class="gplus social                                                        text-hide">
                                    Google+                                                
                                </a>
                                <a href="http://www.glassdoor.com/Reviews/iCore-Reviews-E31837.htm" class="desktop social                                                        text-hide">
                                    Glassdoor                                                
                                </a>
                                <a href="https://www.linkedin.com/company/icore-networks" class="social linkedin                                                        text-hide">
                                    LinkedIn                                                
                                </a>
                            </div>
                            <a href="tel:18669494267" class="phone show-icon">
                            <span class="icon-phone"></span>
                            <span class="phone-label">1-866-949-4267</span>
                            </a>
                            <div class="visible-sm">
                                <a href="http://www.icore.com" class="footer-logo">
                                <span class="img-logo-alt"></span>
                                </a>
                            </div>
                        </div>
                        <div class="hidden-sm col-xxs-6 col-xs-4 col-md-2 footer-logo-wrapper-xs" style="height: 154px;">
                            <a href="http://www.icore.com" class="footer-logo">
                            <span class="img-logo-alt"></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footer-bottom">
                        <span>© iCore Networks, Inc</span>
                        <a class="" href="http://www.icore.com/privacy/">
                        Privacy                          </a>
                        <a class="" href="http://www.icore.com/wordpress/wp-content/uploads/icore-e911disclosure.pdf" target="_blank">
                        E911 Disclosure                          </a>
                        <a class="" href="http://www.icore.com/legal/">
                        Copyright and Intellectual Property Policy                          </a>
                    </div>
                </div>
            </footer>
        </div>
        <!-- # Sidebar -->
        <div class="sidebar-overlay">
            <aside class="sidebar clearfix">
                <a href="javascript:;" class="sidebar-close icon-close text-hide">Close</a>
                <ul class="sidebar-navi">
                    <li class="current active">
                        <a href="http://www.icore.com/">
                        Home                            </a>
                    </li>
                    <li class="">
                        <a href="http://www.icore.com/solutions/">
                        Solutions                            </a>
                    </li>
                    <li class="">
                        <a href="http://www2.icore.com/solutions/by-industry.aspx">
                        Industries                            </a>
                    </li>
                    <li class="">
                        <a href="http://blog.icore.com/">
                        Blog                            </a>
                    </li>
                    <li class="">
                        <a href="http://www2.icore.com/news-events/calendar-of-events.aspx">
                        Events                            </a>
                    </li>
                    <li class="">
                        <a href="http://www2.icore.com/customer-support/faqs.aspx">
                        FAQs                            </a>
                    </li>
                    <li class="">
                        <a href="http://www.icore.com/support/">
                        Support                            </a>
                    </li>
                    <li class="">
                        <a href="https://icore-openhire.silkroad.com/epostings/index.cfm?version=1">
                        Careers                            </a>
                    </li>
                    <li class="">
                        <a href="http://www2.icore.com/news-events/news-items.aspx">
                        Newsroom                            </a>
                    </li>
                </ul>
                <div class="sidebar-updates">
                    <div class="sidebar-connect">
                        <span class="heading">Connect With Us</span>
                        <div class="social-wrapper">
                            <a href="https://www.facebook.com/iCoreNetworks" class="icon-facebook">
                            Facebook                            </a>
                            <a href="https://twitter.com/iCore_Networks" class="icon-twitter">
                            Twitter                            </a>
                            <a href="https://plus.google.com/+icorenetworks/posts" class="icon-gplus">
                            Google+                            </a>
                            <a href="https://www.linkedin.com/company/icore-networks" class="icon-linkedin">
                            LinkedIn                            </a>
                            <a href="http://www.glassdoor.com/Reviews/iCore-Reviews-E31837.htm" class="icon-desktop">
                            Glassdoor                            </a>
                        </div>
                    </div>
                </div>
                <footer class="sidebar-footer">
                    <ul class="list-unstyled">
                        <li class="">
                            <a href="http://www.icore.com/privacy/">
                            Privacy                            </a>
                        </li>
                        <li class="">
                            <a href="http://www.icore.com/wordpress/wp-content/uploads/icore-e911disclosure.pdf" target="_blank">
                            E911 Disclosure                            </a>
                        </li>
                        <li class="">
                            <a href="http://www.icore.com/legal/">
                            Copyright and Intellectual Property Policy                            </a>
                        </li>
                        <li>© iCore Networks, Inc </li>
                    </ul>
                </footer>
            </aside>
        </div>
                
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/vendor/grids.min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/main.js"></script>
    </body>
</html>